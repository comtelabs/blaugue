import axios from "axios";
import {handleResponse} from "@/functions/utils";
import {Article, UpdateArticle, WriteArticle} from "@/functions/interfaces/articles";
import presentToast from "@/functions/ionic/toasts";
import {BlResponse} from "@/functions/interfaces/responses";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}


function refreshTokenForArticles() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

function typeWithEmoji(type: string) {
    switch (type) {
        case "Infau":
            return "Infau 📢"
        case "Faumille":
            return "Faumille 🏡"
        case "Anecdaute":
            return "Anectdaute 🧪"
        case "Auventure":
            return "Auventure 🤠"
        case "Spaurt":
            return "Spaurt 🏃‍♂️"
        case "Éducaution":
            return "Éducaution 🎓"
        case "Technaulogie":
            return "Technaulogie 💾"
        case "Boustifaille":
            return "Boustifaille 🍽️"
        case "Vaucances":
            return "Vaucances 🏖️"
        case "Auctivités":
            return "Auctivités 🧩"
        default:
            return type + " 💭"
    }
}


function getList() {
    return JSON.parse(localStorage.getItem('userList') || "[]") || []
}


function addToList(id: string) {
    let list: Array<string>
    try {
        list = getList()
    } catch (e) {
        list = []
    }
    if (!list.includes(id)) {
        list.push(id)
        presentToast('bottom', 'Article ajouté à la liste de laucture !', 'success')
    } else {
        list.splice(list.indexOf(id), 1)
        presentToast('bottom', 'Article supprimé de la liste de laucture !', 'warning')
    }
    localStorage.setItem('userList', JSON.stringify(list))
}


async function deleteArticle(id: string): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.delete(url, options))
}


async function getArticle(id: string): Promise<Article> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}


async function getPublicArticle(id: string): Promise<Article> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/public/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}

async function getLast10Articles(nickname: string = ""): Promise<Array<Article>> {
    let url = `${import.meta.env.VITE_BACKEND_URL}/paustes/last10`
    if (nickname != "") {
        url += `/${nickname}`
    }
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}

async function getLast10PublicArticles(): Promise<BlResponse & Array<Article>> {
    let url = `${import.meta.env.VITE_BACKEND_URL}/paustes/last10Public`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}

async function getUserArticles(nickname: string): Promise<Array<Article>> {
    let url = `${import.meta.env.VITE_BACKEND_URL}/paustes/name/${nickname}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}

async function createArticle(article: WriteArticle): Promise<BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes`
    let customHeaders = headers
    customHeaders['Content-Type'] = 'application/json'
    const options = {
        headers: customHeaders
    }
    return await handleResponse(axios.post(url, article, options))
}

async function updateArticle(id: string, article: UpdateArticle): Promise<BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/${id}`
    let customHeaders = headers
    customHeaders['Content-Type'] = 'application/json'
    const options = {
        headers: customHeaders
    }
    return await handleResponse(axios.patch(url, article, options))
}

async function manageArticleCensure(id: string, censured: boolean): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/admin/${id}`
    let customHeaders = headers
    customHeaders['Content-Type'] = 'application/json'
    const options = {
        headers: customHeaders
    }
    const data = {
        censured: censured
    }
    return await handleResponse(axios.patch(url, data, options))
}

export {
    getArticle,
    getPublicArticle,
    getLast10Articles,
    getLast10PublicArticles,
    getUserArticles,
    createArticle,
    typeWithEmoji,
    addToList,
    getList,
    deleteArticle,
    updateArticle,
    manageArticleCensure,
    refreshTokenForArticles
}
