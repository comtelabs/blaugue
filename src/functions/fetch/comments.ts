import axios from "axios";
import {handleResponse} from "@/functions/utils";
import {Comment} from "@/functions/interfaces/comments";
import {BlResponse} from "@/functions/interfaces/responses";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}

function refreshTokenForComments() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

async function addComment(id: string, content: string): Promise<Comment & BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/caummentes`
    const data = {
        "pausteId": id,
        "author": localStorage.getItem('userId'),
        "content": content
    }
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}


async function deleteComment(id: string): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/caummentes/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.delete(url, options))
}


async function updateComment(id: string, content: string): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/caummentes/${id}`
    const data = {
        content: content
    }
    const options = {
        headers: headers
    }
    return await handleResponse(axios.patch(url, `content=${content}`, options))
}


export {
    addComment,
    deleteComment,
    updateComment,
    refreshTokenForComments
}
