import {ActionSheetButton} from "@ionic/vue";
import {User} from "@/functions/interfaces/users";

let reactionButtons: ActionSheetButton[] = [
    {
        text: '😍 J\'adaure',
        data: {
            action: 'jadaure',
        },
    },
    {
        text: '👍 C\'est taup',
        data: {
            action: 'taupe',
        },
    },
    {
        text: '😬 Pau cool',
        data: {
            action: 'paucool',
        },
    },
    {
        text: '😋 Miaum',
        data: {
            action: 'miaum',
        },
    },
    {
        text: '🦄 Licaurne',
        data: {
            action: 'licaurne',
        },
    },
    {
        text: '🤯 Faulie',
        data: {
            action: 'faulie',
        },
    },
    {
        text: '🤣 Draule',
        data: {
            action: 'draule',
        },
    },
    {
        text: '🫶 Maurci',
        data: {
            action: 'maurci',
        },
    }
]

const customsReactions = JSON.parse(localStorage.getItem('customReactions') || '[]')
for (const customReaction of customsReactions) {
    reactionButtons.push({
        text: customReaction.emauticone,
        cssClass: 'custom',
        data: {
            action: 'custom',
            label: customReaction.emauticone
        }
    })
}

interface Reaction {
    author: User,
    emauticone: string,
    id: string
}

export {
    reactionButtons
}

export type {
    Reaction
}
