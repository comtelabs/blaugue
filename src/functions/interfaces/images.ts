interface Image {
    urls: {
        120: string,
        360: string,
        1024: string,
        default: string
    },
    id: string
}

export type {
    Image
}
