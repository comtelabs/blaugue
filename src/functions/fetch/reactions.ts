import axios from "axios";
import {handleResponse} from "@/functions/utils";
import {BlResponse} from "@/functions/interfaces/responses";
import {Reaction} from "@/functions/interfaces/reactions";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}

function refreshTokenForReactions() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

function emote(reaction: string) {
    switch (reaction) {
        case 'jadaure':
            return "😍 J'adaure"
        case 'taupe':
            return "👍 C'est taup"
        case 'licaurne':
            return "🦄 Licaurne"
        case 'faulie':
            return "🤯 Faulie"
        case 'paucool':
            return "😬 Pau cool"
        case 'miaum':
            return "😋 Miaum"
        case 'draule':
            return " 🤣 Draule"
        case 'maurci':
            return " 🫶 Maurci"
        default:
            return "😍 J'adaure"
    }
}


function getEmoji(reaction: string) {
    const split = reaction.split(' ')
    for (const string of split) {
        if (string !== '') return string
    }
}


async function addArticleReaction(id: string, emote: string): Promise<Reaction & BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/sociaules/pauste`
    const data = {
        "emauticone": emote,
        "pausteId": id,
        "author": localStorage.getItem('userId')
    }
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}


async function addCommentReaction(id: string, emote: string): Promise<Reaction & BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/sociaules/caummente`
    const data = {
        "emauticone": emote,
        "caummenteId": id,
        "author": localStorage.getItem('userId')
    }
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}

async function deleteReaction(id: string): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/sociaules/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.delete(url, options))
}


export {
    addArticleReaction,
    addCommentReaction,
    emote,
    deleteReaction,
    getEmoji,
    refreshTokenForReactions
}
