import presentToast from "@/functions/ionic/toasts";
import axios, {AxiosResponse} from "axios";
import {login, refreshToken} from "@/functions/fetch/account";
import {Share} from "@capacitor/share";
import {Clipboard} from '@capacitor/clipboard';
import router from "@/router";
import {refreshTokenForBlaugers} from "@/functions/fetch/blauguer";
import {refreshTokenForReactions} from "@/functions/fetch/reactions";
import {refreshTokenForComments} from "@/functions/fetch/comments";
import {refreshTokenForImages} from "@/functions/fetch/images";
import {refreshTokenForArticles} from "@/functions/fetch/articles";
import {refreshTokenForInteractions} from "@/functions/fetch/interactions";


async function toBase64(file: Blob): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.result) resolve(reader.result.toString())
        }
        reader.onerror = () => reject;
        reader.readAsDataURL(file);
    })
}

async function goTo(path: string): Promise<void> {
    await router.push(path)
}

async function share(title: string, content: string, url: string, dialog: string): Promise<void> {
    try {
        await Share.share({
            title: title,
            text: content,
            url: url,
            dialogTitle: dialog
        })
    } catch (e) {
        await Clipboard.write({
            string: url
        });
        await presentToast('bottom', 'Lien copié dans le presse papier !', 'success')
    }

}

async function hashPassword(password: string): Promise<string> {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const data = encoder.encode(password);
    const hash = await crypto.subtle.digest('SHA-256', data);
    return decoder.decode(hash)
}

async function handleResponse(request: Promise<AxiosResponse>): Promise<any> {
    const acceptedCodes = ['200', '201']

    return await request.then((response) => {
        const json = response.data;
        json["code"] = response.status;
        if (!acceptedCodes.includes(json["code"].toString())) {
            throw "Request unknown code"
        }
        return json
    }).catch(async (err) => {
        if (err instanceof TypeError) {
            return {"code": 200, "message": ""}
        }
        if (err.request.status == 401) {
            if (localStorage.getItem('userStayConnected') === 'true') {
                const creds = JSON.parse(localStorage.getItem('userCreds') || "{}")
                await presentToast('bottom', 'Reconnexion...', 'normal', 500)
                return await login(creds['nickname'], creds['password']).then(response => {
                    if (['200', '201', 200, 201].includes(response['code'])) {
                        localStorage.setItem('userToken', response['access_token'])
                        refreshToken()
                        refreshTokenForBlaugers()
                        refreshTokenForReactions()
                        refreshTokenForArticles()
                        refreshTokenForComments()
                        refreshTokenForImages()
                        refreshTokenForInteractions()
                        const newRequest = err.config
                        newRequest.headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
                        return handleResponse(axios.request(err.config))
                    }
                })
            }
            await presentToast('bottom', 'Accès refusé, connexion requise.', 'warning')
            setTimeout(() => {
                location.href = '/login'
            }, 2000)
        } else if (err.request.status == 400) {
            await presentToast('bottom', 'Une erreur est survenue, assurez-vous d\'avoir bien rempli le formulaire.', 'warning')
        } else {
            await presentToast('bottom', 'Une erreur est survenue.', 'danger')
        }
        return {"code": 1055, "message": err}
    })

}

export {
    hashPassword,
    handleResponse,
    toBase64,
    share,
    goTo
}
