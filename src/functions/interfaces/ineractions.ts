enum InteractionType {
    comment ='caummente',
    social = 'sociaule',
    socialOfComment = 'sociauleOfComment'
}

interface Interaction {
    authorsNickname: string,
    authorsAvatar: string,
    pausteId: string,
    pausteTitle: string,
    content: string,
    interactionType: InteractionType,
    interactionCreatedAt: string,
    id: null
}

export type {
    Interaction
}
