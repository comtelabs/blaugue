import axios from "axios";
import {handleResponse} from "@/functions/utils";
import {BlResponse} from "@/functions/interfaces/responses";
import {Interaction} from "@/functions/interfaces/ineractions";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}

function refreshTokenForInteractions() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}


async function getLast30Interactions(): Promise<Interaction[] & BlResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/paustes/last30Interauctions`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}


export {
    getLast30Interactions,
    refreshTokenForInteractions
}
