import axios from "axios";
import {handleResponse} from "@/functions/utils";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}

function refreshTokenForImages() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

async function deleteImage(id: string): Promise<JSON> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/imauges/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.delete(url, options))
}

export {
    deleteImage,
    refreshTokenForImages
}
