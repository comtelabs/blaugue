interface User {
    nickname: string,
    email: string,
    avatar: string,
    roles: string[],
    location: {
        city: string,
        country: string,
        coordinates: number[]
    },
    id: string
}

interface UpdateUser {
    nickname: string,
    email: string,
    avatar: string,
    location: {
        city: string,
        country: string,
        coordinates: number[]
    },
}

interface CreateUser {
    nickname: string,
    email: string,
    avatar: string,
    password: string
    location: {
        city: string,
        country: string,
        coordinates: number[]
    },
}


interface LoginUser {
    nickname: string,
    password: string
}

export type {
    User,
    CreateUser,
    LoginUser,
    UpdateUser
}
