import {User, LoginUser, CreateUser, UpdateUser} from "@/functions/interfaces/users";
import axios from "axios";
import {handleResponse, hashPassword} from "@/functions/utils";
import {BlImageUploadResponse, BlResponse} from "@/functions/interfaces/responses";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}

function refreshToken() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

async function fetchAccount(): Promise<User> {
    let name = localStorage.getItem('userName')
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/name/${name}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}


async function updateAccount(changes: UpdateUser): Promise<BlResponse> {
    let id = localStorage.getItem('userId')
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.patch(url, changes, options))
}

async function updatePassword(newPassword: string): Promise<BlResponse> {
    let id = localStorage.getItem('userId')
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/password/${id}`
    const options = {
        headers: headers
    }
    const data = {
        'password': await hashPassword(newPassword)
    }
    return await handleResponse(axios.patch(url, data, options))
}


async function deleteAccount(): Promise<BlResponse> {
    let id = localStorage.getItem('userId')
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/${id}`
    return await handleResponse(axios.delete(url))
}

async function createAccount(nickname: string, email: string, password: string): Promise<BlResponse> {
    if (password == '') {
        return await new Promise((resolve, _) => {
            // @ts-ignore
            resolve({"code": 1025, "message": "Champs de formulaire invalides."});
        })
    }
    const data: CreateUser = {
        nickname: nickname,
        email: email,
        avatar: "/public/assets/avatar.png",
        password: await hashPassword(password),
        location: {
            city: "Blaugue",
            country: "France",
            coordinates: [0, 0]
        }
    }
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}

async function login(nickname: string, password: string): Promise<BlResponse> {
    const data: LoginUser = {
        nickname: nickname,
        password: await hashPassword(password),
    }
    const url = `${import.meta.env.VITE_BACKEND_URL}/auth/login`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}

async function uploadImage(data: FormData): Promise<BlImageUploadResponse> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/imauges/upload`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.post(url, data, options))
}

export {
    fetchAccount,
    updateAccount,
    updatePassword,
    deleteAccount,
    createAccount,
    login,
    refreshToken,
    uploadImage
}
