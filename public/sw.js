// const appURL = 'blaugue.camponovo.xyz'
const backendURL = 'backtoblaugue.camponovo.xyz'
const appURL = 'localhost:8100'

const isOtherWebsite = (url) => {
    return !url.includes(appURL) || url.includes(backendURL);

}

const addResourcesToCache = async (resources) => {
    const cache = await caches.open("v1");
    await cache.addAll(resources);
};

const cacheFirst = async (request) => {
    const canBeCached = request.url.includes(`${backendURL}/imauges`)
    const responseFromCache = await caches.match(request);
    if (responseFromCache !== undefined) {
        console.log('Cached')
        return responseFromCache
    } else if (canBeCached) {
        console.log(`Uncached ${request.url}, trying to cache`)
        try {
            const response = await fetch(request)
            if (response.ok) await addResourcesToCache([request.clone()])
        } catch (e) {
            console.log(`Failed to cache ${request.url}, ${e}`)
        }
    }
    return await fetch(request)
};

self.addEventListener("install", (event) => {
    event.waitUntil(
        addResourcesToCache([
            "/login",
            "/register",
        ])
    );
});

self.addEventListener("fetch", (event) => {
    if (event.request.url.includes(`${backendURL}/imauges`) && event.request.method === 'GET') {
        event.respondWith(cacheFirst(event.request));
        return
    }
    if (event.request.method !== "GET" || isOtherWebsite(event.request.url)) {
        return
    }
    event.respondWith(cacheFirst(event.request));
})
