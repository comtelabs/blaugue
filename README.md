<p align="center">
 <img width="500" src=".gitlab/header.png" alt="Blaugue header">
</p>

# Face de Blaugue
Blaugue is a micro-blogging platform, made for families to share stories, travels and memories... 

Blaugue frontend.

## Documentation

### Setup

#### Install dependencies
**Please use Nodejs 16 at least**

```shell
npm i -g @ionic/cli && npm i
```

#### Configuration
.env
```dotenv
VITE_BACKEND_URL=your backend url   
```

### Start
```shell
ionic serve
```


### Production
```shell
ionic build && vite
```

### Docker
- **Build**
```shell
docker build -t blaugue .
```
- **Run**
```shell
docker run -p 8100:8100 blaugue
```

## Acknowledgement

- Code under [Cecill V2.1](LICENSE), by [Silvère <silvere@comtelabs.fr>](https://comtelabs.fr) & [Armand <armand@camponovo.xyz>](https://www.camarm.dev) CAMPONOVO
