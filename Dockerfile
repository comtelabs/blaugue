FROM node:16-alpine

# create destination directory
RUN mkdir -p /usr/src/website
WORKDIR /usr/src/website

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /usr/src/website/
RUN npm install -g npm@latest
RUN npm install
RUN npm i -g @ionic/cli
RUN ionic build

EXPOSE 8100
CMD [ "npm", "run", "start" ]
