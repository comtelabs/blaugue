import {createRouter, createWebHistory} from '@ionic/vue-router';
import {RouteRecordRaw} from 'vue-router';
import Router from "@/views/Router.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/register',
    component: () => import('@/views/Register.vue'),
  },
  {
    path: '/p/:id',
    component: () => import('@/components/ReadArticle.vue'),
  },
  {
    path: '/edit/:id',
    component: () => import('@/views/Edit.vue'),
  },
  {
    path: '/u/:name',
    component: () => import('@/components/BlauguerProfile.vue'),
  },
  {
    path: '/',
    component: Router,
    children: [
      {
        path: '',
        redirect: '/public'
      },
      {
        path: 'home',
        component: () => import('@/views/Home.vue'),
      },
      {
        path: 'account',
        component: () => import('@/views/Account.vue'),
      },
      {
        path: 'feed',
        component: () => import('@/views/Feed.vue'),
      },
      {
        path: 'search',
        component: () => import('@/views/Search.vue'),
      },
      {
        path: 'public',
        component: () => import('@/views/Public.vue'),
      }
    ]
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
