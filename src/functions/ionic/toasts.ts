import { toastController } from "@ionic/vue";

async function presentToast(position: 'top' | 'middle' | 'bottom', message: string, type: 'success' | 'danger' | 'warning' | 'normal' = 'normal', duration: number = 5000) {
    const toast = await toastController.create({
        message: message,
        duration: duration,
        position: position,
        cssClass: type
    });

    await toast.present();
}

export default presentToast