import axios from "axios";
import {handleResponse} from "@/functions/utils";
import {User} from "@/functions/interfaces/users";

const token = localStorage.getItem('userToken')

var headers = {
    "Accept": "*/*",
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": `Bearer ${token}`
}


function refreshTokenForBlaugers() {
    headers['Authorization'] = `Bearer ${localStorage.getItem('userToken')}`
}

async function getUser(name: string): Promise<User> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/name/${name}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}


async function getUserById(id: string | User | {}): Promise<User> {
    const url = `${import.meta.env.VITE_BACKEND_URL}/blauguers/${id}`
    const options = {
        headers: headers
    }
    return await handleResponse(axios.get(url, options))
}

export {
    getUser,
    getUserById,
    refreshTokenForBlaugers
}
