
function registerWorker() {
    if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js", { scope: "/" })
    }
}

export {
    registerWorker
}