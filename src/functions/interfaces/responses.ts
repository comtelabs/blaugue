interface BlResponse {
    code: string | number,
    message: string,
    [key: string]: any
}

interface BlImageUploadResponse extends BlResponse {
    urls?: {
        120: string,
        360: string,
        1024: string,
        default: string,
    }
}

export type {
    BlResponse,
    BlImageUploadResponse
}
