import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.comtelabs.blaugue',
  appName: 'blaugue',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
