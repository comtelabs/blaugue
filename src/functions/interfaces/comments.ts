import {User} from "@/functions/interfaces/users";
import {Reaction} from "@/functions/interfaces/reactions";

interface Comment {
    content: string,
    author: User,
    sociaules: Reaction[],
    createdAt: string,
    id: string
}

export type {
    Comment
}
