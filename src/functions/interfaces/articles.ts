import {User} from "@/functions/interfaces/users";
import {Image} from "@/functions/interfaces/images";
import {Reaction} from "@/functions/interfaces/reactions";
import {Comment} from "@/functions/interfaces/comments";

interface WriteArticle {
    title: string,
    content: string,
    author: string,
    private: boolean,
    categauries: string[]
}


interface UpdateArticle {
    title: string,
    content: string,
    author: string,
    private: boolean,
    categauries: string[]
}

interface Article {
    id: string,
    title: string,
    content: string,
    author: User,
    createdAt: string,
    private: boolean,
    censured: boolean,
    categauries: string[],
    caummentes: Comment[],
    sociaules: Reaction[],
    imauges: Image[],
    code?: string | number,
    location: {
        city: string,
        country: string
    }
}


export type {
    Article,
    WriteArticle,
    UpdateArticle
}
